﻿using System;
using UnityEngine;

namespace Game.Controls
{
    public class PlayerInputHandler : MonoBehaviour
    {
        public Action OnLeftClickClicked;
        public Action OnRightClickClicked;
        
        public Vector3 moveVector = new Vector3();
        private void Update()
        {
            HandleMoveInput();
            HandleMouseInput();
        }

        private void HandleMouseInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnLeftClickClicked?.Invoke();
            }
            
            if (Input.GetMouseButtonDown(1))
            {
                OnRightClickClicked?.Invoke();
            }
        }

        private void HandleMoveInput()
        {
            if (Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S))
            {
                moveVector.z = 1;
            }
            else if (Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.W))
            {
                moveVector.z = -1;
            }
            else
            {
                moveVector.z = 0;
            }

            if (Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A))
            {
                moveVector.x = 1;
            }
            else if (Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
            {
                moveVector.x = -1;
            }
            else
            {
                moveVector.x = 0;
            }
        }
    }
}
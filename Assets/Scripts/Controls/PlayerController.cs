﻿using Game.Animation;
using Game.Core;
using UnityEngine;

namespace Game.Controls
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private int _speed = 25;

        [SerializeField] private float _turnRate = 1f;
        [SerializeField]private LayerMask _layerMask;
        private Camera _mainCamera;
        private Transform _cameraOrientationTransform;
        private CharacterController _characterController;
        private PlayerInputHandler _inputHandler;
        private AnimationController _animationController;

        void Awake()
        {
            _characterController = GetComponent<CharacterController>();
            _animationController = GetComponent<AnimationController>();
            _inputHandler = GetComponent<PlayerInputHandler>();
            _mainCamera = Camera.main;
            _cameraOrientationTransform = _mainCamera.transform.GetChild(0);
        }

        void Update()
        {
            MoveLogic();
            LookAtCursor();
        }

        public Vector3 GetDirectionFromMouseLocation()
        {
            var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction *300f, Color.red, 2f);
            if(Physics.Raycast(ray.origin, ray.direction,  out var info, 300f, _layerMask))
            {
                Debug.DrawRay(info.point, Vector3.up*1f, Color.blue, 2f);
                //To ensure it flies in a straight line
                var myPos = transform.position;
                var targetPos = info.point;
                targetPos.y = myPos.y;
                return (targetPos- myPos ).normalized;
            }

            Debug.Log("No Hit");
            return transform.forward;
        }

        private void MoveLogic()
        {
            var moveVector = _inputHandler.moveVector;
            if (!moveVector.Equals(Vector2.zero))
            {
                var cameraRelativeForward = _cameraOrientationTransform.forward * moveVector.z;
                var cameraRelativeRight = _cameraOrientationTransform.right * moveVector.x;

                var finalMoveVector = (cameraRelativeForward + cameraRelativeRight).normalized;
                //It is normalized to avoid the player moving faster when moving in multiple directions at once.

                //Either of the 2 below will work, but characterController
                //will prevent collision issues, without needing to have a RigidBody.

                _characterController.Move(finalMoveVector * (_speed * Time.deltaTime));
                //transform.position += finalMoveVector * (Speed * Time.deltaTime * SpeedModifier);

                var animationMoveVector = transform.InverseTransformDirection(finalMoveVector);
                AnimatorMoveInput(animationMoveVector.x, animationMoveVector.z);
            }
            else
            {
                //to lerp to 0 over time.
                AnimatorMoveInput(0, 0);
            }
        }

        private void AnimatorMoveInput(float x, float y)
        {
            _animationController.AnimatorMoveInput(x,y);
        }

        void LookAtCursor()
        {
            Vector2 positionOnScreen = _mainCamera.WorldToViewportPoint(transform.position);
            Vector2 mouseOnScreen = _mainCamera.ScreenToViewportPoint(Input.mousePosition);
            AdjustToResolution();


            var angle = AngleBetweenTwoPoints(mouseOnScreen, positionOnScreen);
            var yRotation = _mainCamera.transform.rotation.eulerAngles.y - angle + 90;
            //The +90 is because the final result is always off by -90 for some reason
            var playerRot = transform.rotation.eulerAngles;
            var desiredRotation = Quaternion.Euler(new Vector3(playerRot.x, yRotation, playerRot.z));
            
            var result = Quaternion.RotateTowards(transform.rotation, desiredRotation, _turnRate);
            transform.rotation = result;

            //A dot product might be more accurate. Need to test it.    
            float AngleBetweenTwoPoints(Vector3 pointOne, Vector3 pointTwo)
            {
                return Mathf.Atan2(pointOne.y - pointTwo.y, pointOne.x - pointTwo.x) * Mathf.Rad2Deg;
            }

            //This is needed because of different monitor aspect ratios/resolutions, the end result was always slightly wrong
            //Because of the fact that WorldToViewportPoint and ScreenToViewportPoint functions both
            //assume that the x and y directions are of equal size.
            void AdjustToResolution()
            {
                float width = Screen.width;
                float height = Screen.height;
                var ratio = width / height;
                mouseOnScreen.x = mouseOnScreen.x * ratio;
                positionOnScreen.x = positionOnScreen.x * ratio;
            }
        }
        
        private Vector2 AdjustToResolution( Vector2 vector)
        {
            float width = Screen.width;
            float height = Screen.height;
            var ratio = width / height;
            vector.x = vector.x * ratio;
            return vector;
        }

        
        //Animation Events
        void FootR()
        {
            //just to avoid annoying error message in logs.
        }

        void FootL()
        {
            //just to avoid annoying error message in logs.
        }

        void PlayStep()
        {
            //just to avoid annoying error message in logs.
        }

    }
}

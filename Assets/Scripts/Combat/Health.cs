﻿using System;
using UnityEngine;

namespace Gane.Combat
{
    public class Health : MonoBehaviour
    {

        private float _health;
        private float _maxHealth;
        
        public Action<float, float> OnHealthChanged;
        public Action OnDeath;
        public float CurrentHealth => _health;

        public void TakeDamage(float amount)
        {
            if (amount < 0)
            {
                return;
            }
            _health -= amount;
            OnHealthChanged.Invoke(_health, _maxHealth);
            if (_health <= 0)
            {
                _health = 0;
                SetDead();
            }
        }

        public bool IsAlive()
        {
            if (_health > 0)
            {
                return true;
            }
            return false;
        }

        private void SetDead()
        {
            OnDeath?.Invoke();
        }
    }
}
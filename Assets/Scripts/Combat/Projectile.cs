﻿using System;
using System.Collections;
using Game.Pools;
using Game.Sound;
using Gane.Combat;
using Gane.Sound;
using UnityEngine;

namespace Game.Combat
{
    
    [Serializable]
    public class Projectile : MonoBehaviour
    {
        [SerializeField] public float _speed = 5f;

        [Tooltip("Homing Missile On/Off")]
        [SerializeField] private bool _isHoming = false;
        [Tooltip("On = Hits the Environment, Off = goes through objects, " +
                 "currently an object without a Health Component is considered Environment")]
        [SerializeField] private bool _hitEnvironment = true;
        
        [Tooltip("Can only hit the target set")]
        [SerializeField] private bool _OnlyHitSpecifiedTarget;

        [Tooltip("Delay before projectile is destroyed in seconds, 0 for instant")] 
        [SerializeField] private float _despawionDelayOnImpact = 0f;
        
        [Header("Effects")]
        [SerializeField] private ParticleSystem _hitEffect;
        [SerializeField] private AudioObject _hitSound;
        
        
        
        
        private float _maxDistance;
        private float _damage = 0f;
        private int _ownerTransformInstanceID;
        private Transform _target;

        private bool _isLaunched = false;
        private bool _hasHit = false;
        
        private AudioHandler _audioHandler;
        private Projectile _myPrefab;

        public void SetData(ProjectileSettings projectileSettings)
        {
            _maxDistance = projectileSettings.MaxDistance;
            _damage = projectileSettings.Damage;
            _audioHandler = projectileSettings.AudioHandler;
            _ownerTransformInstanceID = projectileSettings.OwnerInstanceId;
            _myPrefab = projectileSettings.PrefabToSpawn;
        }
        
        public void Launch(Transform target)
        {
            _target = target;
            LookAtTarget();
            SetLifeTime();
            _isLaunched = true;
        }

        public void Launch(Vector3 forwardDirection)
        {
            transform.forward = forwardDirection;
            SetLifeTime();
            _isLaunched = true;
        }

        public void Update()
        {
            if (!_isLaunched)
            {
                return;
            }

            if (_hasHit)
            {
                return;
            }

            if (_isHoming)
            {
                LookAtTarget();
            }

            MoveForward();
        }

        void MoveForward()
        {
            transform.position += transform.forward * (_speed * Time.deltaTime);
        }

        void LookAtTarget()
        {
            transform.LookAt(_target);
        }

        private void OnTriggerEnter(Collider other)
        {
            CollisionLogic(other);
        }

        private void OnCollisionEnter(Collision other)
        {
            CollisionLogic(other.collider);
        }

        private void CollisionLogic(Collider other)
        {
            //Debug.Log(other.name + "  " + other.gameObject.layer );
            if (!_isLaunched)
            {
                return;
            }

            if (other.gameObject.layer == 10 || other.gameObject.layer  == 9)
            {
                return;
            }

            var targetHealth = other.GetComponent<Health>();
            var isAlive = false;
            var isEnvironment = false;
            if (targetHealth != null)
            {
                isAlive = IsAlive(targetHealth);
            }
            else
            {
                isEnvironment = true;
                //Can change to tag compare for destructible environment? 
            }

            if (isEnvironment && !_hitEnvironment)
            {
                return;
            }

            if (isEnvironment && _hitEnvironment)
            {
                PlayHitEffect();
                PlayHitSound();
                ReturntoPool();
                return;
            }

            if (isAlive && !_OnlyHitSpecifiedTarget)
            {
                targetHealth.TakeDamage(CalculateDamage());
                PlayHitEffect();
                PlayHitSound();
                ReturntoPool();
                return;
            }

            if (isAlive && _OnlyHitSpecifiedTarget && other.transform.GetInstanceID() == _target.GetInstanceID())
            {
                targetHealth.TakeDamage(CalculateDamage());
                PlayHitEffect();
                PlayHitSound();
                ReturnToPoolNow();
                return;
            }
        }

        private float CalculateDamage()
        {
            return _damage;
        }

        private void SetLifeTime()
        {
            ReturnToPool(_maxDistance/_speed);
        }


        private void PlayHitSound()
        {
            if (_hitSound == null) { return; }
            if (_audioHandler == null) { return; }
            _audioHandler.PlayAudioObject(_hitSound);
        }
        
        private void PlayHitEffect()
        {
            if (_hitEffect == null)
            {
                return;
            }
            var item =  ObjectPool<ParticleSystem>.GetItem(_hitEffect, transform.position, transform.rotation);
            ObjectPool<ParticleSystem>.ReturnToPool(_hitEffect, item, _hitEffect.main.duration);
        }

        private static bool IsAlive(Health targetHealth)
        {
            if (targetHealth.IsAlive())
            {
                return true;
            }

            return false;
        }

        private void ReturnToPoolNow()
        {
            _isLaunched = false;
            _hasHit = false;
            _maxDistance = 0;
            _target = null;
            _damage = 0;
            _ownerTransformInstanceID = 0;
            ObjectPool<Projectile>.ReturnToPool(_myPrefab, this);
        }

        private void ReturntoPool()
        {
            ReturnToPool(_despawionDelayOnImpact);
        }

        private void ReturnToPool(float delay)
        {
            StartCoroutine(ReturnToPool_CR(delay));
        }
        
        private IEnumerator ReturnToPool_CR(float delay)
        {
            yield return new WaitForSeconds(delay);
            ReturnToPoolNow();
        }
    }
    
    [Serializable]
    public class ProjectileSettings
    {
        public float Damage;
        public float MaxDistance;
        public int OwnerInstanceId;
        public AudioHandler AudioHandler;
        public Projectile PrefabToSpawn;
        
        public void SetData(float damage, float maxDistance, int ownerInstanceID, AudioHandler audioHandler, Projectile prefab)
        {
            Damage = damage;
            MaxDistance = maxDistance;
            OwnerInstanceId = ownerInstanceID;
            AudioHandler = audioHandler;
            PrefabToSpawn = prefab;
        }
    }
}

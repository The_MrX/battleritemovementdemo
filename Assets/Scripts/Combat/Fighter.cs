﻿using Game.Combat;
using Game.Controls;
using Game.Pools;
using Game.Sound;
using UnityEngine;

namespace Combat
{
    public class Fighter : MonoBehaviour
    {
        private PlayerInputHandler _playerInputHandler;
        private AudioHandler _audioHandler;
        private PlayerController _playerController;
        [SerializeField] private Projectile _projectilePrefab;
        [SerializeField] private Transform _projectileSpawnPoint;
        
        private void Awake()
        {
            _playerInputHandler = GetComponent<PlayerInputHandler>();
            _audioHandler = GetComponent<AudioHandler>();
            _playerController = GetComponent<PlayerController>();
            _playerInputHandler.OnLeftClickClicked += ShootProjectile;
        }

        void ShootProjectile()
        {
            var projectile = ObjectPool<Projectile>.GetItem(_projectilePrefab, _projectileSpawnPoint.position, Quaternion.identity);
            
            var projectileSettings = new ProjectileSettings();
            projectileSettings.SetData(10, 30f, transform.GetInstanceID(), 
                _audioHandler, _projectilePrefab);
            
            projectile.SetData(projectileSettings);
            projectile.Launch(_playerController.GetDirectionFromMouseLocation());
            //projectile.Launch(transform.forward);
        }
    }
}
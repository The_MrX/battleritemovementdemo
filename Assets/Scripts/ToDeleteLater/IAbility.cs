﻿// using System;
// using UnityEngine;
//
// namespace Game.Abilities
// {
//     public interface IAbility 
//     {
//         Action OnSpellActivated { get; set; }
//         GameObject Owner { get; }
//
//         bool IsInitialized { get; }
//         bool IsFired { get; }
//
//         void SetOwner(GameObject owner);
//         
//          void Tick();
//
//          void Init();
//
//          void Start();
//
//          void Finish();
//     }
// }
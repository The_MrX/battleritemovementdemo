﻿// using System;
// using UnityEngine;
//
// namespace Game.Abilities
// {
//     public class BaseProjectile : MonoBehaviour, IAbility, IProjectile
//     {
//         [SerializeField]private LayerMask _layerMask;
//         [SerializeField]private GameObject _projectilePrefab;
//         [SerializeField]private ParticleSystem _impactVFX;
//         
//         private float _projectileSpeed = 1f;
//         private GameObject _projectileGO;
//         private float _projectileSpeed1;
//         private Action _onSpellActivated;
//         private GameObject _owner;
//         private bool _isInitialized;
//         private bool _isFired;
//
//
//         #region Properties
//         public GameObject ProjectilePrefab
//         {
//             get => _projectileGO;
//             set => _projectileGO = value;
//         }
//
//         public float ProjectileSpeed
//         {
//             get => _projectileSpeed1;
//             set => _projectileSpeed1 = value;
//         }
//
//         public void ChangeProjectSpeed()
//         {
//             throw new NotImplementedException();
//         }
//
//         public void CheckTrigger(Collider other)
//         {
//             if ( (_layerMask &(1<<other.gameObject.layer)) != 0)
//             {
//                 OnImpact();
//             }
//         }
//
//         public void OnImpact()
//         {
//             //Apply Damage
//             //Despawn
//             
//         }
//
//         public void SpawnVFX()
//         {
//             throw new NotImplementedException();
//         }
//
//         public void DeSpawnVFX()
//         {
//             throw new NotImplementedException();
//         }
//
//         public Action OnSpellActivated
//         {
//             get => _onSpellActivated;
//             set => _onSpellActivated = value;
//         }
//
//         public GameObject Owner
//         {
//             get => _owner;
//         }
//
//         public bool IsInitialized
//         {
//             get => _isInitialized;
//         }
//
//         public bool IsFired
//         {
//             get => _isFired;
//         }
//         #endregion
//
//         public virtual void Init()
//         {
//             
//         }
//
//         public virtual void Start()
//         {
//             _projectileGO = Instantiate(_projectilePrefab, Owner.transform.position, Owner.transform.rotation,
//                 Owner.transform);
//         }
//
//         public virtual void SetOwner(GameObject owner)
//         {
//             
//         }
//
//         public virtual void Tick()
//         {
//             if (Time.timeScale > 0)
//             {
//                 MoveForward();
//             }
//         }
//
//         public virtual void Finish()
//         {
//             Destroy(_projectileGO);
//         }
//
//         protected virtual void MoveForward()
//         {
//             transform.position += transform.forward * (_projectileSpeed * Time.deltaTime);
//         }
//
//         private void OnTriggerEnter(Collider other)
//         {
//             CheckTrigger(other);
//         }
//         
//         
//     }
// }
﻿using Gane.Sound;
using UnityEngine;

namespace Game.Sound
{
    public class AudioHandler : MonoBehaviour
    {
        private AudioSource _audioSource;

        void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public AudioClip GetMainAudioClip
        {
            get { return _audioSource.clip; }
        }

        public void PlayAudioObject(AudioObject audioObject)
        {
            if (audioObject == null) { return; }
            if (audioObject._audioClip == null ) { return; }
            _audioSource.PlayOneShot(audioObject._audioClip, audioObject._volume);
            
        }

        public void Stop()
        {
            _audioSource.Stop();
        }

        public void Pause()
        {
            _audioSource.Pause();
        }

        public void UnPause()
        {
            _audioSource.UnPause();
        }

        public void SetMainAudioClip(AudioClip audioClip)
        {
            _audioSource.clip = audioClip;
        }

        public void Mute()
        {
            _audioSource.mute = true;
        }

        public void UnMute()
        {
            _audioSource.mute = false;
        }
        

    }
}
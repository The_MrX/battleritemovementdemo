﻿using System;
using UnityEngine;

namespace Gane.Sound
{
    
    [Serializable]
    public class AudioObject
    {
        public AudioClip _audioClip;
        [Range(0,1)]public float _volume = 0.7f;

    }
}
﻿using Cinemachine;
using UnityEngine;

namespace Game.Canera
{
    public class CursorOffset : CinemachineExtension
    {
        
        [SerializeField]private float XClamp = 1;
        [SerializeField]private float ZClamp = 1;
        [SerializeField]private float sensitivity = 1;


        protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
        {
            if (Camera.main == null)
            {
                return;
            }
            
            var currentMousePosition =  Camera.main.ScreenToViewportPoint(Input.mousePosition); //value will be between 0 to 1 for each axis.
            currentMousePosition += new Vector3(-0.5f, -0.5f, 0);
            currentMousePosition *= 2f;
            currentMousePosition = ClampVector3(currentMousePosition,1, 1, 1 );
            currentMousePosition.z = currentMousePosition.y;
            currentMousePosition.y = 0;
            
            var offset = (currentMousePosition * sensitivity);
            offset = ClampVector3( offset, XClamp, 0, ZClamp);

            state.PositionCorrection += offset;
            //state.RawPosition += offset;
        }

        private Vector3 ClampVector3(Vector3 vector, float xClamp, float yClamp, float zClamp)
        {
            vector.x = Mathf.Clamp(vector.x, -xClamp, xClamp);
            vector.y = Mathf.Clamp(vector.y, -yClamp, yClamp);
            vector.z = Mathf.Clamp(vector.z, -zClamp, zClamp);
            return vector;
        }
    }
}
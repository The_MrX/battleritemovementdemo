﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Pools
{
    public class ObjectPoolHelper : MonoBehaviour
    {
        public static ObjectPoolHelper Singleton;
        
        [SerializeField ]public Transform _defaultParent;
        
        private void Awake()
        {
            if (Singleton != null)
            {
                Destroy(this);
                return;
            }
            Singleton = this;
        }

        public static T InstantiateItem<T>(T prefab) where T : Component
        {
            return Instantiate(prefab);
        }
    }
    
    public static class ObjectPool<T>  where T : Component
    {
        static Dictionary<int, Queue<T>> _pool = new Dictionary<int, Queue<T>>();
        private static T  _cache;

        private static Transform _defaultParent;
        private static Transform tempCache;

        static ObjectPool()
        {
            _defaultParent = ObjectPoolHelper.Singleton._defaultParent;
        }
        
        public static void PreWarm(T prefab, int numOfInstances)
        {
            var id = prefab.transform.GetInstanceID();
            if (!_pool.ContainsKey(id))
            {
                _pool.Add(id, new Queue<T>());
            }

            for (int i = 0; i < numOfInstances; i++)
            {
                _pool[id].Enqueue(ObjectPoolHelper.Instantiate(prefab));
            }
        }
        
       
        public static T GetItem(T prefab)
        {
            var id = prefab.transform.GetInstanceID();
            if (_pool.ContainsKey(id))
            {
                if (_pool[id].Count == 0)
                {
                    return ObjectPoolHelper.Instantiate(prefab);
                }

                _cache =_pool[id].Dequeue();
                _cache.gameObject.SetActive(true);
                return _cache;
            }

            _pool.Add(id, new Queue<T>());
            return ObjectPoolHelper.Instantiate(prefab);
        }


        private static IEnumerator ReturnToPool_CR(T prefab, T instance, float delay)
        {
            yield return new WaitForSeconds(delay);
            ReturnToPool(prefab, instance);
        }

       public static bool ReturnToPool(T prefab, T instance)
       {
           var id = prefab.transform.GetInstanceID();
           instance.gameObject.SetActive(false);
           if (_pool.ContainsKey(id))
           {
               _pool[id].Enqueue(instance);
               return true;
           }

           Debug.Log("Queue for object not found");
           return false;
       }

       public static void ReturnToPool(T prefab, T instance, float delay)
        {
            ObjectPoolHelper.Singleton.StartCoroutine(ReturnToPool_CR(prefab,instance,delay));
        }

        public static T GetItem(T prefab, Vector3 position, Quaternion rotation, Transform parent = null)
        {
            var item = GetItem(prefab);
            tempCache = item.transform;
            tempCache.position = position;
            tempCache.rotation = rotation;
            if (parent == null)
            {
                tempCache.parent = _defaultParent;
            }
            else
            {
                tempCache.parent = parent;
            }
            tempCache = null;
            return item;
        }
    }
}
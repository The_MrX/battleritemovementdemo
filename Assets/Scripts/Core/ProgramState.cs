﻿using Game.Controls;
using UnityEngine;

namespace Game.Core
{
    public class ProgramState : MonoBehaviour
    {
        public static ProgramState Singleton;
        private PlayerInputHandler _playerInputHandler;
        private PlayerController _playerController;

        public PlayerController Controller => _playerController;
        public PlayerInputHandler InputHandler => _playerInputHandler;

        private void Awake()
        {
            Singleton = this;
            _playerInputHandler = FindObjectOfType<PlayerInputHandler>();
            _playerController = FindObjectOfType<PlayerController>();
        }
    }
}
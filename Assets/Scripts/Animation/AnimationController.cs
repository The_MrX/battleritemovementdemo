﻿using UnityEngine;

namespace Game.Animation
{
    public class AnimationController : MonoBehaviour
    {
        private Animator animator;
        
        [Header("Animation Damp Time to avoid jerky movements")] [SerializeField]
        private float animationDampTime = 0.15f;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }
        
        public void AnimatorMoveInput(float x, float y)
        {
            animator.SetFloat("X", x, animationDampTime, Time.deltaTime);
            animator.SetFloat("Y", y, animationDampTime, Time.deltaTime);
        }
    }
}